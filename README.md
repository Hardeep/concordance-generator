# Concordance Generator
Generates an alphabetical list of words in a file along with number of occurrences of each word, and line number the word occurred in.

Note: (a) This application requires JDK 1.5 or later. (b) The Concordance generator is case-insensitive. 
For example: This and this are counted as 2 occurrences. (c) A period followed by a space is assumed to be the end line.
