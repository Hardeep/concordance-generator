package com.hm;

import java.io.File;
import java.util.Map;
import java.util.TreeMap;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.io.FileNotFoundException;

/**
 * Generates an alphabetical list of words in a file along with # of occurrences
 * of each word, and line # the word occurred in.
 *
 * Note: (a) This application requires JDK 1.5 or later. (b) The Concordance
 * generator is case-insensitive. For example: This and this are counted as 2
 * occurrences. (c) A period followed by a space is assumed to be the end line.
 *
 * @author hardeep
 */
public class ConcordanceGenerator {

    /**
     * Main method. Takes the file (to parse) path as the argument.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        if (args.length == 1 && args[0] != null && args[0].length() > 1) {
            generate(args[0]);
        } else {
            System.out.println("Usage: ConcordanceGenerator <FILE_PATH>");
        }
    }

    /**
     * Parses the file and populates the data required to print the alphabetical
     * list.
     *
     * @param file
     */
    private static void generate(String file) {
        Scanner fileScanner = null;

        // match end of sentence
        Pattern sentencePattern = Pattern.compile("(?<!Mr|MR|Ms|MS|Dr|DR)(?<!Mrs|MRS)(?<!Prof|PROF)\\. ");
        TreeMap<String, String> tableOfWords = new TreeMap();

        assert new File(file).exists();

        try {
            fileScanner = new Scanner(new File(file));
        } catch (FileNotFoundException fnfe) {
            System.err.println("File not found (" + file + "), details: " + fnfe);
        }

        if (fileScanner != null) {
            int lineNumber = 1;
            for (String line : sentencePattern.split(fileScanner.nextLine())) {
                Scanner lineScanner = new Scanner(line);
                Pattern wordPattern = Pattern.compile("\\.? ");
                //System.out.println(line);
                for (String word : wordPattern.split(lineScanner.nextLine())) {
                    // ignore #s
                    if (!word.matches(".*\\d.*")) {
                        String wordLower = word.toLowerCase();
                        String data = tableOfWords.get(wordLower);
                        if (data == null) {
                            data = Integer.toString(lineNumber);
                        } else {
                            data = data + "," + lineNumber;
                        }
                        tableOfWords.put(wordLower, data);
                    }
                }
                lineNumber++;
            }
        }

        // print output
        printConcordance(tableOfWords);

    }

    /**
     * Prints the alphabetical list of all word occurrences.
     *
     * @param hm
     */
    private static void printConcordance(Map<String, String> hm) {
        System.out.println("Output\n------");
        for (String word : hm.keySet()) {
            String lines = hm.get(word);
            Pattern p = Pattern.compile("\\,");
            Scanner lineScanner = new Scanner(lines);
            System.out.println(word + "\t\t" + " {" + p.split(lineScanner.nextLine()).length + ":" + lines + "}");
        }
    }

}
